//voor warnings van ycm
typedef unsigned char byte;

//Address SX1509
const byte SX1509_ADDRESS = 0x3E;
//Interrupt pin
const byte interruptPin = 2;
const byte resetPin = 12;
sx1509Class sx1509(SX1509_ADDRESS, resetPin, interruptPin);

const byte button[3] = {13, 14, 15};
const byte fan_pin[6] = {4, 5, 6, 7, 8, 9};

// initialize the library with the number of the sspin
//(or the latch pin of the 74HC595)
LiquidCrystal lcd(10);

class Win {
	public:
		virtual void draw() {};
		virtual void add() {};
		virtual void subtract() {};
		virtual Win *next();
		Win *nxt;
};

class All : public Win {
	public:
		virtual void draw();
};

class FanMode : public Win {
	public:
		virtual void draw();
		virtual void add();
		virtual void subtract();
		static byte mode;
};

class Fan : public Win {
	public:
		virtual void draw();
		virtual void add();
		virtual void subtract();
		virtual Win *next();
		static unsigned short getRPM(const byte);
		static unsigned short avgRPM(const byte*, const byte);
		byte num, pwm;
};

class Tmp : public Win {
	public:
		virtual void draw();
		byte pin;
		static byte getTmp(byte);
};

class Off : public Win {
	public:
		virtual void draw();
		virtual void add();
		virtual void subtract();
};

/* TODO: put in cpp maybe */

Win *Win::next()
{
	return nxt;
}

void All::draw()
{
	unsigned long time = millis();
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("Name for a pc");
	lcd.setCursor(0, 1);
	lcd.print("Up: ");
	lcd.print(time / 3600000);
	lcd.print("h");
	lcd.print((time / 60000) % 60);
	lcd.print("m");
	lcd.print((time / 1000) % 60 % 60);
	lcd.print("s");
}

void FanMode::draw()
{
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("Fan mode:");
	lcd.setCursor(0, 1);
	switch(mode)
	{
		case 1:
			lcd.print("Grouped");
			break;
		case 2:
			lcd.print("All");
			break;
		default:
			lcd.print("Individual");
			break;
	}
}

void FanMode::add()
{
	if(FanMode::mode < 2)
		FanMode::mode++;
}

void FanMode::subtract()
{
	if(FanMode::mode > 0)
		FanMode::mode--;
}

byte FanMode::mode = 0;

void Fan::draw() 
{
	//lcd.clear();
	unsigned short rpm;
	switch(FanMode::mode) //berekenen voor clear() anders flicker :(
	{
		case 1:
			rpm = avgRPM(&fan_pin[num], 2);
			break;
		case 2:
			rpm = avgRPM(&fan_pin[num], 6);
			break;
		default:
			rpm = getRPM(fan_pin[num]);
			break;
	}
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("Fan");
	lcd.setCursor(13, 0);
	lcd.print("RPM");
	lcd.setCursor(0, 1);
	lcd.print("Pct");
	lcd.setCursor(4, 1);
	lcd.print(pwm/2.55);
	lcd.setCursor(9, 1);
	lcd.print("(   )");
	lcd.setCursor(10, 1);
	lcd.print(pwm);
	lcd.setCursor(8, 0);
	lcd.print("     ");
	lcd.setCursor(8, 0);
	lcd.print(rpm);
	switch(FanMode::mode) //berekenen voor clear() anders flicker :(
	{
		case 1:
			lcd.setCursor(3, 0);
			lcd.print(num);
			lcd.setCursor(4, 0);
			lcd.print(((Fan *)nxt)->num);
			break;
		case 2:
			lcd.setCursor(0, 0);
			lcd.print("All fans");
			break;
		default:
			lcd.setCursor(3, 0);
			lcd.print(num);
			break;
	}
}

void Fan::add()
{
	if(pwm < 255)
	{
		switch(FanMode::mode)
		{
			case 2:
				if(num < 5)
					nxt->add();
				break;
			case 1:
				if((num % 2) == 0)
					nxt->add();
				break;
			default:
				break;
		}
		pwm += 15;
		sx1509.pwm(num, pwm);
	}
}

void Fan::subtract()
{
	if(pwm > 0)
	{
		switch(FanMode::mode)
		{
			case 2:
				if(num < 5)
					nxt->subtract();
				break;
			case 1:
				if((num % 2) == 0)
					nxt->subtract();
				break;
			default:
				break;
		}
		pwm -= 15;
		sx1509.pwm(num, pwm);
	}
}

Win *Fan::next()
{
	switch(FanMode::mode)
	{
		case 1:
			return nxt->nxt;
		case 2:
			if(num < 5)
				return nxt->next();
			else
				return nxt;
		default:
			return nxt;
	}
}

unsigned short Fan::getRPM(const byte pin)
{
	unsigned long pulseDuration = pulseIn(pin, LOW, 100000);
	double frequency = 1000000/pulseDuration;
	return frequency*30;
}

unsigned short Fan::avgRPM(const byte *pins, const byte amount)
{
	unsigned short avg = 0;
	for(byte i = 0; i < amount; i++)
	{
		avg += getRPM(pins[i]);
	}
	return (avg / amount);
}

void Tmp::draw()
{
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("TMP1: n/a");
	lcd.setCursor(0, 1);
	lcd.print("TMP2: n/a");
}

byte Tmp::getTmp(byte pin)
{
	return pin;
}

void Off::draw()
{
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("Lights:");
	lcd.setCursor(0, 1);
	if(digitalRead(3) == HIGH)
		lcd.print("OFF");
	else
		lcd.print("ON");
}

void Off::add()
{
	digitalWrite(3, HIGH);
}

void Off::subtract()
{
	digitalWrite(3, LOW);
}
