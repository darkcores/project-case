#include <SPI.h>
#include <LiquidCrystal.h>

#include <Wire.h>
#include <sx1509_library.h>

#include "codeV2.h"

Win *wptr;
All overview;
FanMode fanmode;
Fan fan[6];
Tmp temp;
Off off;

byte counter = 0;

void setup()
{
	// set up the LCD's number of columns and rows:
	lcd.begin(16, 2);

	//init SX1509
	sx1509.init();

	//pins
	sx1509.configClock(INTERNAL_CLOCK, OUTPUT, 0xE, 1);	
	sx1509.debounceConfig(7);
	for(byte i = 0; i < 3; i++)
	{
		sx1509.pinDir(button[i], INPUT);
		sx1509.writePin(button[i], HIGH);
		sx1509.enableInterrupt(button[i], FALLING);
		sx1509.debounceEnable(button[i]);
	}
	for (byte i = 0; i < 6; i++)
	{
		sx1509.ledDriverInit(i, 6.15, LOGARITHMIC);
		sx1509.pwm(i, 255);
		pinMode(fan_pin[i], INPUT);
		digitalWrite(fan_pin[i], HIGH);
		fan[i].num = i;
		fan[i].pwm = 120;
		fan[i].add();
		fan[i].subtract();
	}

	//set up list
	wptr = &overview;
	overview.nxt = &fanmode;
	fanmode.nxt = &fan[0];
	for(byte i = 1; i < 6; i++)
		fan[i-1].nxt = &fan[i];
	fan[5].nxt = &temp;
	temp.nxt = &off;
	off.nxt = &overview;

	//interrupt on pin 2 (INT0)
	//pinMode(interruptPin, INPUT);
	//attachInterrupt(0, buttonISR, FALLING);
}

void reset()
{
	sx1509.reset(1);
	//init SX1509
	sx1509.init();

	//pins
	sx1509.configClock(INTERNAL_CLOCK, OUTPUT, 0xE, 1);	
	sx1509.debounceConfig(7);
	for(byte i = 0; i < 3; i++)
	{
		sx1509.pinDir(button[i], INPUT);
		sx1509.writePin(button[i], HIGH);
		sx1509.enableInterrupt(button[i], FALLING);
		sx1509.debounceEnable(button[i]);
	}
	for (byte i = 0; i < 6; i++)
	{
		sx1509.ledDriverInit(i, 6.15, LOGARITHMIC);
		sx1509.pwm(i, 255);
		fan[i].add();
		fan[i].subtract();
	}
	counter = 0;
}

void loop()
{
	if(digitalRead(interruptPin) == 0)
	{
		buttonISR();
	}
	wptr->draw();
	counter++;
	delay(450);
	if(counter == 127) //sx1509 keeps resetting/stops working
		reset();
}

void buttonISR()
{
	unsigned int interruptSource = sx1509.interruptSource(); //i2c gaat niet in ISR
	if(interruptSource & (1<<button[0])) //add
		wptr->add();
	else if(interruptSource & (1<<button[1])) //subtract
		wptr->subtract();
	else if(interruptSource & (1<<button[2])) //next
		wptr = wptr->next();
	//lcd.clear();
}
